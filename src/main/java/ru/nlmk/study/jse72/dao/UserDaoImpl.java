package ru.nlmk.study.jse72.dao;

import org.springframework.stereotype.Component;
import ru.nlmk.study.jse72.dao.connection.ConnectionManagerImpl;
import ru.nlmk.study.jse72.model.User;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;

@Component
public class UserDaoImpl implements UserDao{
    @Override
    public boolean addUser(User user) {
        Connection connection = new ConnectionManagerImpl().getConnection();
        PreparedStatement preparedStatement = null;
        try {
            preparedStatement = connection.prepareStatement(
                    "INSERT INTO users (login, password, role)" +
                            " values (?, ?, ?)");
            preparedStatement.setString(1, user.getLogin());
            preparedStatement.setString(2, user.getPassword());
            preparedStatement.setString(3, user.getRole());
            preparedStatement.executeQuery();
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return true;
    }

    @Override
    public List<User> getAllUsers() {
        return null;
    }

    @Override
    public void setAdmin(Integer id) {

    }
}
