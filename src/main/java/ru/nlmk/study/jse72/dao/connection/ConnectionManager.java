package ru.nlmk.study.jse72.dao.connection;

import java.sql.Connection;

public interface ConnectionManager {
    public Connection getConnection();
}
