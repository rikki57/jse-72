package ru.nlmk.study.jse72.dao;

import ru.nlmk.study.jse72.model.User;

import java.util.List;

public interface UserDao {
    public boolean addUser(User user);

    List<User> getAllUsers();

    void setAdmin(Integer id);
}
