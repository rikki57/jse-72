package ru.nlmk.study.jse72.service;

public interface UserService {
    public boolean addUser(String login, String password, String role);
}
