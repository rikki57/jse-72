package ru.nlmk.study.jse72.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import ru.nlmk.study.jse72.dao.UserDao;
import ru.nlmk.study.jse72.model.User;

@Service
public class UserServiceImpl implements UserService{
    private BCryptPasswordEncoder bcryptEncoder;
    private UserDao userDao;

    @Autowired
    public UserServiceImpl(BCryptPasswordEncoder bcryptEncoder, UserDao userDao) {
        this.bcryptEncoder = bcryptEncoder;
        this.userDao = userDao;
    }

    @Override
    public boolean addUser(String login, String password, String role) {
        String cryptPassword = bcryptEncoder.encode(password);
        User newUser = new User(0, login, cryptPassword, role);
        userDao.addUser(newUser);
        return false;
    }
}
