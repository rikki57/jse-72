package ru.nlmk.study.jse72.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import ru.nlmk.study.jse72.service.UserService;

@Controller
public class UserController {
    UserService userService;

    @Autowired
    public UserController(UserService userService) {
        this.userService = userService;
    }

    @RequestMapping("/")
    public String index(Model model){
        return "index";
    }

    @RequestMapping("/opened")
    public String opened(Model model){
        return "opened";
    }

    @RequestMapping("/closed")
    public String closed(Model model){
        return "closed";
    }

    @RequestMapping(value = "/addUser", method = RequestMethod.POST)
    public String addUser(@RequestParam(value = "login", required = true) String login,
                          @RequestParam(value = "password", required = true) String password,
                          @RequestParam(value = "role", required = true) String role,
                          Model model){
        userService.addUser(login, password, role);
        return "addUser";
    }

    @RequestMapping(value = "/addUser", method = RequestMethod.GET)
    public String addUser(Model model){
        return "addUser";
    }
}
